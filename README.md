# Apache Docker Environment

This is a simple docker environment for working with Apache 2.4.

## Running the Container

```bash
# Start the server
docker-compose up

# Start in the background
docker-compose up -d

# Tail the Apache logs (if running in background)
docker-compose logs -f apache

# Stop the container
docker-compose stop

# Destroy the container
docker-compose down
```

You can access the server at:  [http://localhost:8080](http://localhost:8080)

## Apache Configuration

Configuration changes should be made in `conf`.  These are automatically mapped to the container.  
Restart the container after making changes:

```bash
docker-compose restart
```

To validate your configuration without restarting:

```bash
docker-compose exec apache apachectl configtest
```

## Apache Document Root

Served files can be modified in `htdocs`.  These are automatically mapped to the container.
